//
//  RadarViewController.m
//  DragonBall
//
//  Created by tanaka.keisuke on 2015/02/20.
//  Copyright (c) 2015年 Classmethod. All rights reserved.
//

#import "RadarViewController.h"
#import "Ball.h"
#import "AppearDragonAnimator.h"
#import "BeaconManager.h"
#import "AVAudioPlayer+Extension.h"
@import CoreLocation;

static NSUInteger const kNumberOfBalls = 7;
static NSTimeInterval const kTimeLimit = 900;  // 制限時間（秒）

@interface RadarViewController () <CLLocationManagerDelegate, UIViewControllerTransitioningDelegate, AVAudioPlayerDelegate>

@property (nonatomic) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet Ball *ball1;
@property (weak, nonatomic) IBOutlet Ball *ball2;
@property (weak, nonatomic) IBOutlet Ball *ball3;
@property (weak, nonatomic) IBOutlet Ball *ball4;
@property (weak, nonatomic) IBOutlet Ball *ball5;
@property (weak, nonatomic) IBOutlet Ball *ball6;
@property (weak, nonatomic) IBOutlet Ball *ball7;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (nonatomic) NSUInteger numberOfFound;
@property (nonatomic) AVAudioPlayer *audioPlayer;
@property (nonatomic) NSTimer *timer;
@property (nonatomic) NSDate *startDate;

@end

@implementation RadarViewController

#pragma mark - Life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setupBeacons];
    [self setupTimer];
    self.numberOfFound = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager
        didRangeBeacons:(NSArray *)beacons
               inRegion:(CLBeaconRegion *)region
{
    CLBeacon *beacon = beacons.firstObject;
    NSLog(@"major %@", beacon.major);
    NSLog(@"minor %@", beacon.minor);

    Ball *dragonBall = [self ballWithIdentifier:region.identifier];

    switch (beacon.proximity) {
        case CLProximityImmediate:
            NSLog(@"CLProximityImmediate");
            [manager stopRangingBeaconsInRegion:region];
            [dragonBall found];
            self.numberOfFound++;
            if (self.numberOfFound >= kNumberOfBalls) [self foundAll];
            break;
        case CLProximityNear:
            NSLog(@"CLProximityNear");
            [dragonBall startAnimationWithProximity:CLProximityNear];
            break;
        case CLProximityFar:
            NSLog(@"CLProximityFar");
            [dragonBall startAnimationWithProximity:CLProximityFar];
            break;
        case CLProximityUnknown:
            NSLog(@"CLProximityUnknown");
            [dragonBall stopAnimation];
            break;
    }
}

#pragma mark - UIViewControllerTransitioningDelegate

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    return [AppearDragonAnimator new];
}

#pragma mark - AVAudioPlayerDelegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"dragonViewControllerID"];
    controller.transitioningDelegate = self;
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark - Private

- (void)setupBeacons
{
    if ([[CLLocationManager class] respondsToSelector:@selector(isMonitoringAvailableForClass:)] &&
        [CLLocationManager isMonitoringAvailableForClass:[CLBeaconRegion class]]) {
        self.locationManager = [CLLocationManager new];
        self.locationManager.delegate = self;

        if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            [self.locationManager requestAlwaysAuthorization];
        }

        for (NSDictionary *beaconInfo in [[BeaconManager sharedManager] beaconsInfo]) {
            CLBeaconRegion *beaconRegion =
            [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:beaconInfo[@"uuid"]]
                                                    major:[beaconInfo[@"major"] integerValue]
                                                    minor:[beaconInfo[@"minor"] integerValue]
                                               identifier:beaconInfo[@"identifier"]];
            [self.locationManager startRangingBeaconsInRegion:beaconRegion];
        }
    }
}

- (void)setupTimer
{
    self.startDate = [NSDate date];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.1f
                                                  target:self
                                                selector:@selector(updateTimer:)
                                                userInfo:nil
                                                 repeats:YES];
}

- (Ball *)ballWithIdentifier:(NSString *)identifier
{
    Ball *ball = nil;

    if ([identifier isEqualToString:kBeaconIdentifier1]) {
        ball = self.ball1;
    } else if ([identifier isEqualToString:kBeaconIdentifier2]) {
        ball = self.ball2;
    } else if ([identifier isEqualToString:kBeaconIdentifier3]) {
        ball = self.ball3;
    } else if ([identifier isEqualToString:kBeaconIdentifier4]) {
        ball = self.ball4;
    } else if ([identifier isEqualToString:kBeaconIdentifier5]) {
        ball = self.ball5;
    } else if ([identifier isEqualToString:kBeaconIdentifier6]) {
        ball = self.ball6;
    } else if ([identifier isEqualToString:kBeaconIdentifier7]) {
        ball = self.ball7;
    }

    return ball;
}

- (void)foundAll
{
    [self.timer invalidate];
    self.timer = nil;

    NSTimeInterval foundSoundPlayTime = 3.0f;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(foundSoundPlayTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.audioPlayer = [AVAudioPlayer audioPlayerWithSoundName:@"found_all"];
        self.audioPlayer.delegate = self;
        [self.audioPlayer play];
        [self foundAllAnimationWithCompletion:^{
            [UIView animateWithDuration:0.25f
                                  delay:0
                                options:kNilOptions
                             animations:^{
                                 self.view.alpha = 0;
                             } completion:nil];
        }];
    });
}

- (void)foundAllAnimationWithCompletion:(dispatch_block_t)completion
{
    [self blinkWithDuration:1.0f times:2 completion:^(BOOL finished) {
        [self blinkWithDuration:0.5f times:4 completion:^(BOOL finished) {
            [self blinkWithDuration:0.25f times:6 completion:^(BOOL finished) {
                if (completion) completion();
            }];
        }];
    }];
}

- (void)blinkWithDuration:(NSTimeInterval)duration times:(NSInteger)times completion:(void (^)(BOOL finished))completion
{
    [UIView animateWithDuration:duration / 2
                          delay:0
                        options:kNilOptions
                     animations:^{
                         self.view.alpha = 0;
                     } completion:^(BOOL finished) {
                         [UIView animateWithDuration:duration / 2
                                               delay:0
                                             options:kNilOptions
                                          animations:^{
                                              self.view.alpha = 1.0f;
                                          } completion:^(BOOL finished) {
                                              if (times - 1 > 0) {
                                                  [self blinkWithDuration:duration times:(times - 1) completion:^(BOOL finished) {
                                                      if (completion) completion(finished);
                                                  }];
                                              } else {
                                                  if (completion) completion(finished);
                                              }
                                          }];
                     }];
}

- (void)updateTimer:(NSTimer *)timer
{
    NSDate *currentDate = [NSDate date];
    NSTimeInterval elapsedTime = [currentDate timeIntervalSinceDate:self.startDate];
    NSTimeInterval remainingTime = kTimeLimit - elapsedTime;
    NSDate *timerDate = [NSDate dateWithTimeIntervalSince1970:remainingTime];

    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"mm:ss"];

    NSString *timeString = [dateFormatter stringFromDate:timerDate];

    if ([timeString isEqualToString:@"00:00"]) {
        [timer invalidate];
        self.timeLabel.textColor = [UIColor lightGrayColor];
        [self showReturnMessage];
    }

    self.timeLabel.text = timeString;
}

- (void)showReturnMessage
{
    [[[UIAlertView alloc] initWithTitle:@"Time's up"
                                message:@"戻ってきてください"
                               delegate:nil
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}

@end
