//
//  Ball.h
//  DragonBall
//
//  Created by tanaka.keisuke on 2015/02/20.
//  Copyright (c) 2015年 Classmethod. All rights reserved.
//

#import <UIKit/UIKit.h>
@import CoreLocation;

@interface Ball : UIView

- (void)found;

- (void)startAnimationWithProximity:(CLProximity)proximity;

- (void)stopAnimation;

@end
