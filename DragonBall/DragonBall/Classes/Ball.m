//
//  Ball.m
//  DragonBall
//
//  Created by tanaka.keisuke on 2015/02/20.
//  Copyright (c) 2015年 Classmethod. All rights reserved.
//

#import "Ball.h"
#import "AVAudioPlayer+Extension.h"
@import AudioToolbox;
@import AVFoundation;

static CGFloat const kNearAnimationDuration = 0.25f;
static CGFloat const kFarAnimationDuration = 0.5f;

@interface Ball ()

@property (nonatomic) AVAudioPlayer *audioPlayer;

@end

@implementation Ball

#pragma mark - Life cycle

- (void)awakeFromNib
{
    [super awakeFromNib];

    [self setup];
}

#pragma mark - Public

- (void)found
{
    [self stopAnimation];
    [self.audioPlayer stop];

    self.backgroundColor = [UIColor orangeColor];

    self.audioPlayer = [AVAudioPlayer audioPlayerWithSoundName:@"found"];
    [self.audioPlayer play];

    for (id star in self.subviews) {
        if ([star isKindOfClass:[UILabel class]]) {
            ((UILabel *)star).textColor = [UIColor redColor];
        }
    }
}

- (void)startAnimationWithProximity:(CLProximity)proximity
{
    [self stopAnimation];
    [self.audioPlayer stop];

    NSTimeInterval duration = (proximity == CLProximityNear) ? kNearAnimationDuration : kFarAnimationDuration;
    SystemSoundID sound = (proximity == CLProximityNear) ? 1011 : kSystemSoundID_Vibrate;
    NSString *soundName = (proximity == CLProximityNear) ? @"near" : @"far";

    self.audioPlayer = [AVAudioPlayer audioPlayerWithSoundName:soundName];
    [self.audioPlayer play];

    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
                     animations:^{
                         self.alpha = 0;
                         AudioServicesPlaySystemSound(sound);   // 「設定」→「サウンド」から「着信あり」をオンにする必要あり
                     } completion:^(BOOL finished) {
                     }];
}

- (void)stopAnimation
{
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView animateWithDuration:kNearAnimationDuration
                     animations:^{
                         self.alpha = 1.0f;
                     }];
}

#pragma mark - Private

- (void)setup
{
    self.layer.cornerRadius = self.bounds.size.height / 2;
    self.layer.borderColor = [UIColor blackColor].CGColor;
    self.layer.borderWidth = 2;
    self.backgroundColor = [UIColor lightGrayColor];

    for (id star in self.subviews) {
        if ([star isKindOfClass:[UILabel class]]) {
            ((UILabel *)star).textColor = [UIColor darkGrayColor];
        }
    }
}

@end
