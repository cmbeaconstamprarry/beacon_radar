//
//  BeaconManager.h
//  DragonBall
//
//  Created by tanaka.keisuke on 2015/02/27.
//  Copyright (c) 2015年 Classmethod. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString *const kBeaconIdentifier1 = @"jp.classmethod.DragonBall.1";
static NSString *const kBeaconIdentifier2 = @"jp.classmethod.DragonBall.2";
static NSString *const kBeaconIdentifier3 = @"jp.classmethod.DragonBall.3";
static NSString *const kBeaconIdentifier4 = @"jp.classmethod.DragonBall.4";
static NSString *const kBeaconIdentifier5 = @"jp.classmethod.DragonBall.5";
static NSString *const kBeaconIdentifier6 = @"jp.classmethod.DragonBall.6";
static NSString *const kBeaconIdentifier7 = @"jp.classmethod.DragonBall.7";

@interface BeaconManager : NSObject

+ (instancetype)sharedManager;

- (NSArray *)beaconsInfo;

@end
