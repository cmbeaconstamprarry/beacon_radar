//
//  BeaconManager.m
//  DragonBall
//
//  Created by tanaka.keisuke on 2015/02/27.
//  Copyright (c) 2015年 Classmethod. All rights reserved.
//

#import "BeaconManager.h"
@import CoreLocation;

static NSString *const kBeaconUUID1 = @"00000000-B9C9-1001-B000-001C4D04DDAB";
static CLBeaconMajorValue const kBeaconMajorValue1 = 0;
static CLBeaconMinorValue const kBeaconMinorValue1 = 0;

static NSString *const kBeaconUUID2 = @"00000000-B9C9-1001-B000-001C4D04DDAB";
static CLBeaconMajorValue const kBeaconMajorValue2 = 0;
static CLBeaconMinorValue const kBeaconMinorValue2 = 1;

static NSString *const kBeaconUUID3 = @"00000000-B9C9-1001-B000-001C4D04DDAB";
static CLBeaconMajorValue const kBeaconMajorValue3 = 0;
static CLBeaconMinorValue const kBeaconMinorValue3 = 2;

static NSString *const kBeaconUUID4 = @"00000000-B9C9-1001-B000-001C4D04DDAB";
static CLBeaconMajorValue const kBeaconMajorValue4 = 0;
static CLBeaconMinorValue const kBeaconMinorValue4 = 3;

static NSString *const kBeaconUUID5 = @"00000000-B9C9-1001-B000-001C4D04DDAB";
static CLBeaconMajorValue const kBeaconMajorValue5 = 0;
static CLBeaconMinorValue const kBeaconMinorValue5 = 4;

static NSString *const kBeaconUUID6 = @"00000000-B9C9-1001-B000-001C4D04DDAB";
static CLBeaconMajorValue const kBeaconMajorValue6 = 1;
static CLBeaconMinorValue const kBeaconMinorValue6 = 0;

static NSString *const kBeaconUUID7 = @"00000000-B9C9-1001-B000-001C4D04DDAB";
static CLBeaconMajorValue const kBeaconMajorValue7 = 1;
static CLBeaconMinorValue const kBeaconMinorValue7 = 2;

@implementation BeaconManager

#pragma mark - Life cycle

+ (instancetype)sharedManager
{
    static BeaconManager *sharedInstance = nil;

    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        sharedInstance = [self new];
    });

    return sharedInstance;
}

#pragma mark - Public

- (NSArray *)beaconsInfo
{
    NSDictionary *info1 = @{ @"uuid": kBeaconUUID1, @"major": @(kBeaconMajorValue1), @"minor": @(kBeaconMinorValue1), @"identifier": kBeaconIdentifier1 };
    NSDictionary *info2 = @{ @"uuid": kBeaconUUID2, @"major": @(kBeaconMajorValue2), @"minor": @(kBeaconMinorValue2), @"identifier": kBeaconIdentifier2 };
    NSDictionary *info3 = @{ @"uuid": kBeaconUUID3, @"major": @(kBeaconMajorValue3), @"minor": @(kBeaconMinorValue3), @"identifier": kBeaconIdentifier3 };
    NSDictionary *info4 = @{ @"uuid": kBeaconUUID4, @"major": @(kBeaconMajorValue4), @"minor": @(kBeaconMinorValue4), @"identifier": kBeaconIdentifier4 };
    NSDictionary *info5 = @{ @"uuid": kBeaconUUID5, @"major": @(kBeaconMajorValue5), @"minor": @(kBeaconMinorValue5), @"identifier": kBeaconIdentifier5 };
    NSDictionary *info6 = @{ @"uuid": kBeaconUUID6, @"major": @(kBeaconMajorValue6), @"minor": @(kBeaconMinorValue6), @"identifier": kBeaconIdentifier6 };
    NSDictionary *info7 = @{ @"uuid": kBeaconUUID7, @"major": @(kBeaconMajorValue7), @"minor": @(kBeaconMinorValue7), @"identifier": kBeaconIdentifier7 };

    return @[ info1, info2, info3, info4, info5, info6, info7 ];
}

@end
