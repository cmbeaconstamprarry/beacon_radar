//
//  AVAudioPlayer+Extension.m
//  DragonBall
//
//  Created by tanaka.keisuke on 2015/02/23.
//  Copyright (c) 2015年 Classmethod. All rights reserved.
//

#import "AVAudioPlayer+Extension.h"

static NSString *const kSoundFileExtension = @"mp3";

@implementation AVAudioPlayer (Extension)

+ (instancetype)audioPlayerWithSoundName:(NSString *)soundName
{
    NSString *path = [[NSBundle mainBundle] pathForResource:soundName ofType:kSoundFileExtension];
    NSURL *url = [NSURL fileURLWithPath:path];
    AVAudioPlayer *audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];

    return audioPlayer;
}

@end
