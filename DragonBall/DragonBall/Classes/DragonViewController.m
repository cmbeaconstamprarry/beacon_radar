//
//  DragonViewController.m
//  DragonBall
//
//  Created by tanaka.keisuke on 2015/02/23.
//  Copyright (c) 2015年 Classmethod. All rights reserved.
//

#import "DragonViewController.h"
#import "AVAudioPlayer+Extension.h"

@interface DragonViewController ()

@property (nonatomic) AVAudioPlayer *audioPlayer;

@end

@implementation DragonViewController

#pragma mark - Life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.audioPlayer = [AVAudioPlayer audioPlayerWithSoundName:@"appear"];
    [self.audioPlayer play];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
