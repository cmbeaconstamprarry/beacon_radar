//
//  AVAudioPlayer+Extension.h
//  DragonBall
//
//  Created by tanaka.keisuke on 2015/02/23.
//  Copyright (c) 2015年 Classmethod. All rights reserved.
//

#import <Foundation/Foundation.h>
@import AVFoundation;

@interface AVAudioPlayer (Extension)

+ (instancetype)audioPlayerWithSoundName:(NSString *)soundName;

@end
