//
//  AppDelegate.h
//  DragonBall
//
//  Created by tanaka.keisuke on 2015/02/20.
//  Copyright (c) 2015年 Classmethod. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
