//
//  AppearDragonAnimator.m
//  DragonBall
//
//  Created by tanaka.keisuke on 2015/02/27.
//  Copyright (c) 2015年 Classmethod. All rights reserved.
//

#import "AppearDragonAnimator.h"

@implementation AppearDragonAnimator

#pragma mark - UIViewControllerAnimatedTransitioning

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext
{
    return 0.25f;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext
{
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *containerView = [transitionContext containerView];
    [containerView addSubview:toViewController.view];
    containerView.layer.transform = CATransform3DMakeScale(0, 0, 1);

    [UIView animateWithDuration:[self transitionDuration:transitionContext]
                     animations:^{
                         containerView.layer.transform = CATransform3DIdentity;
                     }
                     completion:^(BOOL finished) {
                         [transitionContext completeTransition:YES];
                     }];
}

@end
