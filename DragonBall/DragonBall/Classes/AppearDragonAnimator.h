//
//  AppearDragonAnimator.h
//  DragonBall
//
//  Created by tanaka.keisuke on 2015/02/27.
//  Copyright (c) 2015年 Classmethod. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppearDragonAnimator : NSObject <UIViewControllerAnimatedTransitioning>

@end
